/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
} from 'react-native';
import {subCounter, addCounter} from './actions';
import {connect} from 'react-redux';

const App = ({counter, subCounter, addCounter}) => {
  return (
    <SafeAreaView
      style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <View style={{flexDirection: 'row'}}>
        <Button title="-" onPress={() => subCounter()} />
        <Text style={{fontSize: 26}}>{`${counter}`}</Text>
        <Button title="+" onPress={() => addCounter()} />
      </View>
    </SafeAreaView>
  );
};

const mapStateToProps = (state) => {
  return {
    counter: state.counter,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    subCounter: () => {
      dispatch(subCounter());
    },
    addCounter: () => {
      dispatch(addCounter());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
