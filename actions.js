import {SUB_COUNTER, ADD_COUNTER} from './types';

export const subCounter = () => {
  return {
    type: SUB_COUNTER,
  };
};

export const addCounter = () => {
  return {
    type: ADD_COUNTER,
  };
};
